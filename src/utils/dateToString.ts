export function convertDate(dateString: string): string {
	const dateObj = new Date(dateString);
	const months = [
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December",
	];

	// Extract day, month, and year from the date object
	const day = dateObj.getDate().toString().padStart(2, "0"); // Add leading zero
	const month = months[dateObj.getMonth()]; // Get month name
	const year = dateObj.getFullYear().toString(); // Get Year

	// Format the date as "DD Month YYYY"
	const formattedDate = `${day} ${month} ${year}`;
	return formattedDate;
}
