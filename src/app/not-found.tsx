export default function NotFound() {
	return (
		<div className="bg-white w-screen h-screen flex justify-center items-center">
			<h1 className="text-black font-bold text-3xl">404 | Page Not Found</h1>
		</div>
	);
}
