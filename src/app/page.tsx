import Banner from "@/components/banner/Banner";
import ListPost from "@/components/listPost/ListPost";

export default function Home() {
	return (
		<div className="flex flex-col justify-center items-center w-screen">
			<Banner />
			<ListPost />
		</div>
	);
}
