import axios from "axios";
import { useState, useEffect } from "react";
import { BASE_API_URL } from "@/config/config";
import { IFetchListPost, IResponseData } from "@/constants/interface";

const useFetchListPost = ({ pageNum, pageSize, append, sortBy }: IFetchListPost) => {
	const [data, setData] = useState<IResponseData[]>([]);
	const [loading, setLoading] = useState<boolean>(false);
	const [error, setError] = useState<string | null>(null);

	useEffect(() => {
		const fetchData = async () => {
			setLoading(true);
			setError(null);
			const url: string = `${BASE_API_URL}?page[number]=${pageNum}&page[size]=${pageSize}&append[]=small_image&append[]=medium_image&sort=${sortBy}`;
			try {
				console.log("fetching post...");
				const response = await axios.get(url);
				setData(response.data.data as IResponseData[]);
				console.log("finished fetching");
			} catch (err) {
				setError("Failed to fetch data");
			} finally {
				setLoading(false);
			}
		};
		fetchData();
	}, [pageNum, pageSize, append, sortBy]);

	return { data, loading, error };
};

export default useFetchListPost;
