export interface IFetchListPost {
	pageNum: number;
	pageSize: number;
	append: "small_image" | "medium_image";
	sortBy: "published_at" | "-published_at";
}

export interface IResponseData {
	id: number;
	content: string;
	created_at: string;
	deleted_at?: string;
	medium_image: { file_name: string; id: number; mime: string; url: string }[];
	published_at: string;
	slug: string;
	small_image: { file_name: string; id: number; mime: string; url: string }[];
	title: string;
	updated_at: string;
}

export interface IPost {
	id: number;
	published_at: string;
	small_image: { file_name: string; id: number; mime: string; url: string }[];
	title: string;
}

export interface NavbarItem {
	label: string;
	path: string;
}
