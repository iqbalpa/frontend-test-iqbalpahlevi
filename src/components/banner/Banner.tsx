"use client";

import React, { useEffect, useState } from "react";
import ImagePlaceholder from "../../../public/image-placeholder.png";

const Banner: React.FC = () => {
	const [offsetY, setOffsetY] = useState<number>(0);

	const handleScroll = () => {
		setOffsetY(window.scrollY);
	};

	useEffect(() => {
		window.addEventListener("scroll", handleScroll);
		return () => window.removeEventListener("scroll", handleScroll);
	}, []);

	return (
		<div className="relative h-[70vh] w-screen overflow-hidden flex justify-center items-center text-white">
			<div
				className="absolute top-0 left-0 w-full h-full bg-cover bg-center transition-transform duration-100 ease-out"
				style={{
					backgroundImage: `url(${ImagePlaceholder.src}), linear-gradient(to right, rgba(0, 0, 0, 0.8), transparent, rgba(0, 0, 0, 0.8))`,
					backgroundBlendMode: "overlay",
					transform: `translateY(${offsetY * 0.5}px)`,
					clipPath: "polygon(0 0, 100% 0, 100% 65%, 0 95%)",
				}}
			></div>
			<div
				className="relative text-center z-10 transition-transform duration-100 ease-out"
				style={{
					transform: `translateY(${offsetY * 0.3}px)`,
				}}
			>
				<h1 className="text-5xl">Ideas</h1>
				<h3 className="text-lg">Where all our great things begin</h3>
			</div>
		</div>
	);
};

export default Banner;
