"use client";

import useFetchListPost from "@/hooks/useFetchListPost";
import React, { useState, useEffect } from "react";
import {
	MdOutlineNavigateNext,
	MdOutlineNavigateBefore,
	MdKeyboardDoubleArrowRight,
	MdKeyboardDoubleArrowLeft,
} from "react-icons/md";
import Post from "../post/Post";

const ListPost: React.FC = () => {
	const [showPerPage, setShowPerPage] = useState<number>(10);
	const [sortBy, setSortBy] = useState<"published_at" | "-published_at">("-published_at");
	const [currentPage, setCurrentPage] = useState<number>(1);

	const { data, loading, error } = useFetchListPost({
		pageNum: currentPage,
		pageSize: showPerPage,
		append: "small_image",
		sortBy: sortBy,
	});

	const upperbound: number = showPerPage === 10 ? 10 : showPerPage === 20 ? 5 : 2;
	const paginationNumbers: number[] = [];
	for (let i = 1; i <= upperbound; i++) {
		paginationNumbers.push(i);
	}

	const handleShowPerPageChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
		setShowPerPage(Number(event.target.value));
	};
	const handleSortByChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
		setSortBy(event.target.value as "published_at" | "-published_at");
	};
	const handleNextArrow = () => {
		setCurrentPage((prevPage) => {
			if (prevPage === upperbound) {
				return upperbound;
			}
			return prevPage + 1;
		});
	};
	const handlePrevArrow = () => {
		setCurrentPage((prevPage) => {
			if (prevPage === 1) {
				return 1;
			}
			return prevPage - 1;
		});
	};
	const handleDoubleNextArrow = () => {
		setCurrentPage(upperbound);
	};
	const handleDoublePrevArrow = () => {
		setCurrentPage(1);
	};
	const handlePaginationClick = (pageNum: number) => {
		setCurrentPage(pageNum);
	};

	useEffect(() => {
		if (showPerPage === 20 && currentPage >= 5) {
			setCurrentPage(5);
		} else if (showPerPage === 50 && currentPage >= 2) {
			setCurrentPage(2);
		}
	}, [showPerPage, currentPage]);

	if (loading) return <p>Loading...</p>;
	if (error) return <p>Error: {error}</p>;

	return (
		<div className="mt-10 mb-20 flex flex-col w-screen h-full px-10 lg:px-40">
			{/* top */}
			<div className="flex flex-col md:flex-row md:justify-between">
				<p>
					showing {(currentPage - 1) * showPerPage + 1} - {currentPage * showPerPage} of 100
				</p>
				<div className="flex flex-col md:flex-row">
					<div className="flex flex-row items-center justify-between md:justify-normal mt-2 md:mt-0">
						<p className="mr-2">show per page:</p>
						<select
							className="border-[1px] border-gray-500 py-2 px-4 w-24 rounded-3xl"
							name="show-per-page"
							id="show-per-page"
							value={showPerPage}
							onChange={handleShowPerPageChange}
						>
							<option value={10}>10</option>
							<option value={20}>20</option>
							<option value={50}>50</option>
						</select>
					</div>
					<div className="ml-0 md:ml-8 flex flex-row items-center justify-between md:justify-normal mt-2 md:mt-0">
						<p className="mr-2">Sort by:</p>
						<select
							className="border-[1px] border-gray-500 py-2 px-4 w-32 rounded-3xl"
							name="sort-by"
							id="sort-by"
							value={sortBy}
							onChange={handleSortByChange}
						>
							<option value="-published_at">Newest</option>
							<option value="published_at">Oldest</option>
						</select>
					</div>
				</div>
			</div>

			{/* list post */}
			<div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-8 mt-5 lg:mt-10">
				{data.map((post) => {
					return (
						<Post
							key={post.id}
							id={post.id}
							title={post.title}
							small_image={post.small_image}
							published_at={post.published_at}
						/>
					);
				})}
			</div>

			{/* pagination */}
			<div className="mt-5 lg:mt-10 flex justify-center items-center flex-row flex-wrap">
				<MdKeyboardDoubleArrowLeft
					onClick={handleDoublePrevArrow}
					className={`text-2xl hover:cursor-pointer ${currentPage === 1 ? "text-gray-400" : ""}`}
				/>
				<MdOutlineNavigateBefore
					onClick={handlePrevArrow}
					className={`text-2xl hover:cursor-pointer ${currentPage === 1 ? "text-gray-400" : ""}`}
				/>
				{paginationNumbers.map((pageNumber) => (
					<button
						onClick={() => handlePaginationClick(pageNumber)}
						className={`py-2 px-4 rounded-lg ${
							pageNumber === currentPage ? "bg-orangeSuitmedia text-white" : ""
						} mt-2 lg:mt-0 lg:ml-2`}
						key={pageNumber}
					>
						{pageNumber}
					</button>
				))}
				<MdOutlineNavigateNext
					onClick={handleNextArrow}
					className={`text-2xl hover:cursor-pointer ${currentPage === upperbound ? "text-gray-400" : ""}`}
				/>
				<MdKeyboardDoubleArrowRight
					onClick={handleDoubleNextArrow}
					className={`text-2xl hover:cursor-pointer ${currentPage === upperbound ? "text-gray-400" : ""}`}
				/>
			</div>
		</div>
	);
};

export default ListPost;
