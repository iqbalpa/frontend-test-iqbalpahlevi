"use client";

import React, { useState, useEffect } from "react";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { NavbarItem } from "@/constants/interface";
import { Fade as Hamburger } from "hamburger-react";
import { motion } from "framer-motion";

const variants = {
	open: { opacity: 1, x: 0 },
	closed: { opacity: 0, x: "-100%" },
};

const navbarItems: NavbarItem[] = [
	{ label: "Work", path: "/work" },
	{ label: "About", path: "/about" },
	{ label: "Services", path: "/services" },
	{ label: "Ideas", path: "/" },
	{ label: "Careers", path: "/careers" },
	{ label: "Contact", path: "/contact" },
];

const Header: React.FC = () => {
	const [scrollPosition, setScrollPosition] = useState<number>(0);
	const [isScrollingUp, setIsScrollingUp] = useState<boolean>(true);
	const [isOpen, setIsOpen] = useState<boolean>(false);
	const pathname: string = usePathname();

	const handleScroll = () => {
		const currentScrollPosition = window.scrollY;
		if (currentScrollPosition > scrollPosition) {
			setIsScrollingUp(false);
		} else {
			setIsScrollingUp(true);
		}
		setScrollPosition(currentScrollPosition);
	};

	useEffect(() => {
		window.addEventListener("scroll", handleScroll);
		return () => {
			window.removeEventListener("scroll", handleScroll);
		};
	}, [scrollPosition]);

	return (
		<div
			className={`fixed top-0 w-full bg-orangeSuitmedia transition-all duration-300 ease-in-out 
				${isScrollingUp ? "translate-y-0" : "-translate-y-full"} 
				${scrollPosition === 0 ? "opacity-100" : "opacity-80"} 
				z-50`}
		>
			<div className="flex justify-between items-center p-4">
				<h1 className="text-white">suitmedia</h1>
				<div className="flex lg:hidden mr-2">
					<Hamburger toggled={isOpen} toggle={setIsOpen} color="white" />
				</div>
				<ul className="hidden lg:flex space-x-4">
					{navbarItems.map((navItem, index) => (
						<li key={index}>
							<Link
								href={navItem.path}
								className={` text-white relative pb-1 hover:underline hover:underline-offset-8 duration-200 hover:font-bold ${
									pathname === navItem.path ? "underline underline-offset-8 decoration-4" : ""
								}`}
							>
								{navItem.label}
							</Link>
						</li>
					))}
				</ul>
			</div>
			{isOpen && (
				<motion.div
					className="bg-orangeSuitmedia text-white flex flex-col justify-center"
					animate={isOpen ? "open" : "closed"}
					variants={variants}
					transition={{ duration: 3 }}
				>
					{navbarItems.map((item, index) => (
						<Link href={item.path} key={index}>
							<div className="w-full text-center hover:underline hover:underline-offset-4 hover:font-bold duration-200">
								<p className="mb-2">{item.label}</p>
							</div>
						</Link>
					))}
				</motion.div>
			)}
		</div>
	);
};

export default Header;
