import React from "react";
import Image from "next/image";
import { convertDate } from "@/utils/dateToString";
import { IPost } from "@/constants/interface";

const Post: React.FC<IPost> = ({ id, published_at, small_image, title }) => {
	const published: string = convertDate(published_at);

	if (small_image.length === 0) {
		console.log(small_image);
		return null;
	}

	return (
		<div key={id} className="hover:scale-105 duration-200 rounded-lg shadow-2xl w-[20wh] p-5">
			<Image
				src={small_image[0].url}
				alt={small_image[0].file_name}
				width={200}
				height={250}
				loading="lazy"
				className="rounded-t-lg"
			/>
			<p className="text-sm md:text-base text-gray-500 uppercase mt-2">{published}</p>
			<h2 className="text-base md:text-lg font-semibold line-clamp-3">{title}</h2>
		</div>
	);
};

export default Post;
